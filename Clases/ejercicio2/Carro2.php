<?php
//creación de la clase carro
class Carro2{
	//declaracion de propiedades
	public $color;
	public $modelo;

	//añadiendo nuevas propiedades
	public $añoFabricacion;
	private $verifica;

	//declaracion del método verificación
	public function verificacion(){
		if($this->añoFabricacion < 1990){
			$this->verifica = "No";
		}
		if($this->añoFabricacion >= 1990 && $this->añoFabricacion < 2011){
			$this->verifica = "Revision";
		}
		if($this->añoFabricacion > 2010){
			$this->verifica = "Si";
		}
		return $this->verifica;
	}
}

//creación de instancia a la clase Carro
$Carro1 = new Carro2();

if (!empty($_POST)){

	$Carro1->color=$_POST['color'];
	$Carro1->modelo=$_POST['modelo'];

	// obteniendo el valor del elemento html año de fabricacion 
	$Carro1->añoFabricacion=$_POST['añoF'];

}




