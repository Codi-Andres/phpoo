<?php
include_once('transporte.php');

//declaracion de la clase hijo o subclase Carro
class tren extends transporte
{

	private $numero_vagones;

	//declaracion de constructor
	public function __construct($nom, $vel, $com, $vagon)
	{
		//sobreescritura de constructor de la clase padre
		parent::__construct($nom, $vel, $com);
		$this->numero_vagones = $vagon;

	}

	// declaracion de metodo
	public function resumenTren()
	{
		// sobreescribitura de metodo crear_ficha en la clse padre
		$mensaje = parent::crear_ficha();
		$mensaje .= '<tr>
						<td>Numero de vagones:</td>
						<td>' . $this->numero_vagones . '</td>				
					</tr>';
		return $mensaje;
	}
}

//declaracion del mensaje vacio
$mensaje = '';

//creacion del objeto con sus respectivos parametros para el constructor
$tren1= new tren('tren de mercancías','350','diésel','9');

//obteniendo transporte terrestre
if (!empty($_POST)){
	//re validando que el tipo dee transporte sea terrestre
	if ($_POST['tipo_transporte'] == 'ferroviario'){
		$mensaje=$tren1->resumenTren();
	}

}