<?php
include_once('transporte.php');

//declaracion de la clase hijo Barco
class barco extends transporte
{
    private $calado;

    //sobreescritura de constructor
    public function __construct($nom, $vel, $com, $cal)
    {
        parent::__construct($nom, $vel, $com);
        $this->calado = $cal;
    }

    // sobreescritura de metodo
    public function resumenBarco()
    {
        $mensaje = parent::crear_ficha();
        $mensaje .= '<tr>
                    <td>Calado:</td>
                    <td>' . $this->calado . '</td>				
                </tr>';
        return $mensaje;
    }
}

//declaracion del mensaje vacio
$mensaje = '';

//creacion del objeto con sus respectivos parametros para el constructor
$bergantin1= new barco('bergantin','40','na','15');

//obteniendo transporte maritimo
if (!empty($_POST)){
	//re validando que el tipo dee transporte sea maritimo
	if ($_POST['tipo_transporte'] == 'maritimo'){
		$mensaje=$bergantin1->resumenBarco();
	}

}