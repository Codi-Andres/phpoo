<?php
//declaracion de clase token
class token
{
    //declaracion de atributos
    private $nombre;
    private $token;
    //declaracion de metodo constructor
    public function __construct($nombre_front)
    {
        $this->nombre = $nombre_front;
        $this->token = $this->randomLetters();
    }

    //declaracion del metodo mostrar para armar el mensaje con el nombre y token
    public function mostrar()
    {
        return 'Hola ' . $this->nombre . ' se genero tu contraseña ' ;
    }

    //declaracion de metodo destructor
    public function __destruct()
    {
        //destruye token
        $this->token = 'La contraseña '. $this->token.' ha sido destruida';
        echo $this->token;
    }

    //declaracion del metodo generar token para obtener letras aleatorias
    private function randomLetters()
    {
        $caracteres_permitidos = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $longitud = 4;
        return substr(str_shuffle($caracteres_permitidos), 0, $longitud);
    }
}

$mensaje = '';


if (!empty($_POST)) {
    //creacion de objeto de la clase
    $token1 = new token($_POST['nombre']);
    $mensaje = $token1->mostrar();
}


?>