# Curso PHP Orientado a Objetos

## Descripcion
En este repositorio se encuentran las actividades realizadas durante el curso, el cual consta de una pagina web con un menú. Cada opcion del menu ejecuta una funcion diferente con temas relacionados a herencia, clases, objetos, modificadores de acceso y constructores y destructores. 

## Autor
Andres Saul Garzon Oviedo
