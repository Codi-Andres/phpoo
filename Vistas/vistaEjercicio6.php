
<!DOCTYPE html>
<html>

<head>

	<link rel="stylesheet" href="../css/bootstrap.min.css">
	<link rel="stylesheet" href="../css/bootstrap-grid.css">
	<script type="text/javascript" src="../js/bootstrap.min.js"></script>
	<script type="text/javascript" src="../js/jquery-3.4.1.min.js"></script>
	<title>
		Indice
	</title>
</head>

<body>

	<div class="container" style="margin-top: 4em">

		<header>
			<h1>Los transportes</h1>
		</header><br>
		<form method="post">


			<div class="form-group">
				<label for="CajaTexto1">Tipo de transporte:</label>
				<select class="form-control" name="tipo_transporte" id="CajaTexto1">
					<option value='aereo'>Aereo</option>
					<option value='terrestre'>Terrestre</option>
					<option value='maritimo'>Maritimo</option>

					<!-- añadiendo la opcion para transporte ferroviario -->
					<option value='ferroviario'>Ferroviario</option>

				</select>
			</div>



			<button class="btn btn-primary" type="submit">enviar</button>
			<a class="btn btn-link offset-md-8 offset-lg-9 offset-6" href="../index.php">Regresar</a>
		</form>

	</div>
	<div class="container mt-5">
		<h1>Respuesta del servidor</h1>
		<table class="table">
			<thead>
				<tr>
					<th>Transporte</th>
				</tr>
			</thead>
			<tbody>
				<!-- llamando a las clases (con include) y ejecutando el $mensaje -->
				<?php
            switch ($_POST['tipo_transporte']) {
	            case 'aereo':
		            include('../clases/ejercicio6/Avion.php');
		            echo $mensaje;
		            break;
	            case 'terrestre':
		            include('../clases/ejercicio6/Carro.php');
		            echo $mensaje;
		            break;
	            case 'maritimo':
		            include('../clases/ejercicio6/Barco.php');
		            echo $mensaje;
		            break;
					case 'ferroviario':
						include('../clases/ejercicio6/Tren.php');
						echo $mensaje;
						break;
            }
            ?>

			</tbody>
		</table>

	</div>

</body>

</html>